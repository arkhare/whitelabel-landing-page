
var SettingsPanel = {

    initMenu: function () {
        if(SettingsPanel.isFullMenuRequired()){
            $("#jsQualityMenuTitle").click(QualitySelector.events.closeQualityMenu);
            if (!Timeline.SettingsJsonObject.ClosedCaptionsSupported){
                $('#jsCCMenuItem').remove();
            }
            if (!Timeline.SettingsJsonObject.MultiLanguageSupported){
                $('#jsLangMenuItem').remove();
            }
            $('#jsQualitySelectorContainer').hide();
            $('#jsSettingsContainer').show();
        } else {
            $('#jsSettingsContainer').hide();
            $('#jsQualitySelectorContainer').show();
            SettingsPanel.setupQualityOnlyCss();
        }
        SettingsPanel.events.initialise();
        SettingsPanel.enableRequiredMenus();
        SettingsPanel.enableSettingsMenu();
    },

    isFullMenuRequired: function () {
        return Timeline.SettingsJsonObject.ClosedCaptionsSupported || Timeline.SettingsJsonObject.MultiLanguageSupported;
    },

    enableRequiredMenus: function () {

        $('#jsQualitySelectorContainer').hover(QualitySelector.events.qualityButtonHoverInEventHandler, QualitySelector.events.qualityButtonHoverOutEventHandler);

        if(!SettingsPanel.isFullMenuRequired()){
            $('#jsQualitySelectorPopout').hover(QualitySelector.events.qualityButtonHoverInEventHandler, QualitySelector.events.qualityButtonHoverOutEventHandler);
        }

        $('#jsQualityMenuItem').click(SettingsPanel.events.settingsItemClickHandler);
        $('#jsCCMenuItem').click(SettingsPanel.events.settingsItemClickHandler);
    },

    enableSettingsMenu: function () {
        $('#jsSettingsButtonIcon').click(SettingsPanel.events.settingsButtonClickEventHandler);
    },

    setupQualityOnlyCss: function(){
        $('#jsQualityMenuTitle').removeClass('hover');
        $('#jsQualityLeftArrow').remove();
        $('#jsQualityMenuTitleText').removeClass("col-xs-10").addClass("col-xs-12");
    },

    events: {
        /**
         * Link up the events and the event handlers
         */
        initialise: function() {
            $('#jsLangMenuItem').click(SettingsPanel.events.settingsItemClickHandler);
        },

        /**
         * Display the quality selector control when the user hovers over the settings icon
         */
        settingsButtonClickEventHandler: function(e) {
            var popout = $('#jsSettingsButtonPopout');
            if ($('#jsCCSelectorPopout').is(':visible') || $('#jsQualitySelectorPopout').is(':visible') ||
                    $('#jsLanguageSelectorPopout').is(':visible') || popout.is(':visible')){
                $('#jsCCSelectorPopout').hide();
                $('#jsQualitySelectorPopout').hide();
                $('#jsLanguageSelectorPopout').hide();
                popout.hide();
            } else {
                popout.show();
            }
        },

        settingsItemClickHandler: function(e) {
            var setting = $(this).data("setting");
            switch (setting) {
                case "language":
                    $('#jsLanguageSelectorPopout').show();
                    $('#jsSettingsButtonPopout').hide();
                    return;
                case "quality":
                    $('#jsQualitySelectorPopout').show();
                    $('#jsSettingsButtonPopout').hide();
                    return;
                case "closed-captions":
                    $('#jsCCSelectorPopout').show();
                    $('#jsSettingsButtonPopout').hide();
                    return;

            }
        }

    }
};