<div id="jsCCSelectorPopout" class="timeline-control-cc__popout">

    <div id="jsCCMenuTitle" class="cc-menu-title" tabindex="2">
        <div class="float-left">
            <div class="setting-arrow left"></div>
        </div>
        <div class="cc-menu-text">
            <span class="translate" data-translate="CCMenuText">Closed Captions</span>
            <span class="setting-light-text js-cc-text"> (Off)</span>
        </div>
        <div class="clear-both"></div>
    </div>
    <div id="jsCCOffButton" class="timeline-control-cc__item" data-value="off" tabindex="2">
        <div class="cc-tick-container">
            <div id="jsCCOffTick" class="selected-value">✔</div>
        </div>
        <div class="cc-item-text">Off</div>
        <div class="clear-both"></div>
    </div>
    <div id="jsCCOnButton" class="timeline-control-cc__item" data-value="on" tabindex="2">
        <div class="cc-tick-container">
            <div id="jsCCOnTick" class="selected-value">✔</div>
        </div>
        <div class="cc-item-text">On</div>
        <div class="clear-both"></div>
    </div>

</div>